<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>АстраКасса | Загрузки</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__left">
    <div class="stackedit__toc">
      
<ul>
<li><a href="#загрузить-астракассу">Загрузить АстраКассу</a>
<ul>
<li><a href="#версия-для-windows">Версия для Windows</a></li>
<li><a href="#версия-для-android">Версия для Android</a></li>
</ul>
</li>
</ul>

    </div>
  </div>
  <div class="stackedit__right">
    <div class="stackedit__html">
      <h1 id="загрузить-астракассу">Загрузить АстраКассу</h1>
<p>На этой странице выложены последние версии клиентского ПО АстраКассы.</p>
<h2 id="версия-для-windows">Версия для Windows</h2>
<p><a href="https://cloud.astrapos.ru/files/astrapos.zip">Скачать (Версия 2018.12.12)</a><br>
Подходит для:<br>
Windows 7, 8, 8.1, 10 (x86 и x64)</p>
<h2 id="версия-для-android">Версия для Android</h2>
<p><a href="https://cloud.astrapos.ru/files/astrapos.zip">Скачать (Версия 2018.12.12)</a><br>
Подходит для:<br>
Android 4.4+</p>

    </div>
  </div>
</body>

</html>
